package com.epam.javaconcurency.threads;

import com.epam.javaconcurency.entity.Matrix;
import com.epam.javaconcurency.entity.MatrixManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//Class of matrix changing thread
public class MatrixThread extends Thread {
    private final static Logger LOG = LogManager.getLogger(MatrixThread.class);

    private MatrixManager matrixManager;
    private Matrix matrix;
    private int integerToChangeTo;

    public MatrixThread(MatrixManager matrixManager, Matrix matrix, int integerToChangeTo) {
        this.matrixManager = matrixManager;
        this.matrix = matrix;
        this.integerToChangeTo = integerToChangeTo;
    }

    public MatrixManager getMatrixManager() {
        return matrixManager;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public int getIntegerToChangeTo() {
        return integerToChangeTo;
    }

    public void setMatrixManager(MatrixManager matrixManager) {
        this.matrixManager = matrixManager;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }

    public void setIntegerToChangeTo(int integerToChangeTo) {
        this.integerToChangeTo = integerToChangeTo;
    }

    @Override
    public void run() {
        LOG.info("Thread " + Thread.currentThread().getName() + " has replaceable integer " + integerToChangeTo);
        matrixManager.changeElementInMatrixMainDiagonal(matrix, integerToChangeTo);
    }
}
