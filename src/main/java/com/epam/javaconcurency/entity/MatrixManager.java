package com.epam.javaconcurency.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantReadWriteLock;

// Class to manage change of matrix main diagonal elements by multiple threads
public class MatrixManager {
    // Logger initialization
    private final static Logger LOG = LogManager.getLogger(MatrixManager.class);
    // Array of locks to lock changing matrix element
    private AtomicReferenceArray<ReentrantReadWriteLock> locks;

    // Constructor
    public MatrixManager(int numberOfLocks) {
        //Initializing array of locks
        locks = new AtomicReferenceArray<>(numberOfLocks);
        for (int i = 0; i < numberOfLocks; i++) {
            locks.set(i, new ReentrantReadWriteLock(true));
        }
    }

    // Getter
    public AtomicReferenceArray<ReentrantReadWriteLock> getLocks() {
        return locks;
    }

    // Setter
    public void setLocks(AtomicReferenceArray<ReentrantReadWriteLock> locks) {
        this.locks = locks;
    }

    /**
     * Method to change elements in matrix main diagonal
     *
     * @param matrix            matrix which elements need to change
     * @param integerToChangeTo integer which will replace matrix element
     */
    public void changeElementInMatrixMainDiagonal(Matrix matrix, int integerToChangeTo) {
        // Iterating through each row of matrix
        for (int i = 0; i < matrix.getMatrix().length(); i++) {
            // If main diagonal element wasn't changed
            if (matrix.getMatrix().get(i).get(i) == 0) {
                // And if it is not being changed by another thread, try to lock the element
                if (locks.get(i).writeLock().tryLock()) {
                    // Try to change the element
                    try {
                        LOG.debug("Matrix main diagonal element # " + (i + 1) + " is locked by thread " + Thread.currentThread().getName());
                        // Changing matrix element
                        matrix.getMatrix().get(i).set(i, integerToChangeTo);
                        // Wait 100 milliseconds to allow other threads make changes in matrix
                        TimeUnit.MILLISECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        LOG.error(e);
                    } finally {
                        LOG.debug("Matrix main diagonal element # " + (i + 1) + " is changed by thread " + Thread.currentThread().getName() + " and unlocked");
                        // Unlocking the element
                        locks.get(i).writeLock().unlock();
                    }
                }
            }
        }
    }
}
