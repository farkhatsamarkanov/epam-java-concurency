package com.epam.javaconcurency.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicReferenceArray;

//Class to store matrix
public class Matrix {
    //Matrix field
    private AtomicReferenceArray<AtomicIntegerArray> matrix;

    //Constructor
    public Matrix(int matrixSize) {
        matrix = new AtomicReferenceArray<>(matrixSize);
        //Initializing matrix row by row
        for (int i = 0; i < matrixSize; i++) {
            AtomicIntegerArray row = new AtomicIntegerArray(matrixSize);
            for (int j = 0; j < matrixSize; j++) {
                row.set(j, 0);
            }
            matrix.set(i, row);
        }
    }

    //Getter
    public AtomicReferenceArray<AtomicIntegerArray> getMatrix() {
        return matrix;
    }

    //Setter
    public void setMatrix(AtomicReferenceArray<AtomicIntegerArray> matrix) {
        this.matrix = matrix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix1 = (Matrix) o;
        return Objects.equals(getMatrix(), matrix1.getMatrix());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMatrix());
    }

    @Override
    public String toString() {
        StringBuilder toReturn = new StringBuilder();
        for (int row = 0; row < matrix.length(); row++) {
            toReturn.append(matrix.get(row))
                    .append("\n");
        }
        return toReturn.toString();
    }
}
