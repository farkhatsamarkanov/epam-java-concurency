package com.epam.javaconcurency.initialization;

import com.epam.javaconcurency.configuration.Configuration;
import com.epam.javaconcurency.configuration.ConfigurationReaderWriter;
import com.epam.javaconcurency.entity.Matrix;
import com.epam.javaconcurency.entity.MatrixManager;
import com.epam.javaconcurency.threads.MatrixThread;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

//Initialization class
public class FirstTask {
    // Logger initialization
    private final static Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {
        //Reading configuration parameters from XML file
        Configuration configuration = ConfigurationReaderWriter.readConfigurationFromFile();
        // If reading is successful
        if (configuration != null) {
            // Creating matrix instance
            Matrix matrix = new Matrix(configuration.getMatrixSize());
            // Printing out initial matrix
            LOG.info("Initial matrix: \n\n" + matrix);
            // Creating MatrixManager instance
            MatrixManager matrixManager = new MatrixManager(configuration.getMatrixSize());
            // Starting threads to change matrix main diagonal elements
            ExecutorService executorService = Executors.newFixedThreadPool(configuration.getNumberOfThreads());
            for (int i = 0; i < configuration.getNumberOfThreads(); i++) {
                executorService.submit(new MatrixThread(matrixManager, matrix, configuration.getIntegerToReplaceArray()[i]));
            }
            executorService.shutdown();
            // Waiting for all threads complete their tasks
            try {
                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                // Printing out result matrix
                LOG.info("Result matrix: \n\n" + matrix);
            } catch (InterruptedException e) {
                LOG.error(e);
            }
        }
    }
}
