package com.epam.javaconcurency.configuration;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

// Class to store program configuration
public class Configuration implements Serializable {
    // Configuration parameters
    private int matrixSize;
    private int numberOfThreads;
    // Array of integers which will replace matrix main diagonal elements
    private int[] integerToReplaceArray;

    // Constructors
    public Configuration() {
    }

    Configuration(int matrixSize, int numberOfThreads, int[] integerToReplaceArray) {
        this.matrixSize = matrixSize;
        this.numberOfThreads = numberOfThreads;
        this.integerToReplaceArray = integerToReplaceArray;
    }

    // Getters
    public int getMatrixSize() {
        return matrixSize;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public int[] getIntegerToReplaceArray() {
        return integerToReplaceArray;
    }

    // Setters
    public void setMatrixSize(int matrixSize) {
        this.matrixSize = matrixSize;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public void setIntegerToReplaceArray(int[] integerToReplaceArray) {
        this.integerToReplaceArray = integerToReplaceArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Configuration)) return false;
        Configuration that = (Configuration) o;
        return matrixSize == that.matrixSize &&
                numberOfThreads == that.numberOfThreads &&
                integerToReplaceArray == that.integerToReplaceArray;
    }

    @Override
    public int hashCode() {
        return Objects.hash(matrixSize, numberOfThreads, integerToReplaceArray);
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "MATRIX_SIZE=" + matrixSize +
                ", NUMBER_OF_THREADS=" + numberOfThreads +
                ", REPLACEABLE_INTEGER_BOUND=" + Arrays.toString(integerToReplaceArray) +
                '}';
    }
}
