package com.epam.javaconcurency.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

// Class to read and write configuration files
public class ConfigurationReaderWriter {
    // Initialization of log4j2 logger
    private final static Logger LOG = LogManager.getLogger(ConfigurationReaderWriter.class);

    // Constructor
    public ConfigurationReaderWriter() {
    }

    // Method to read configuration from XML file
    public static Configuration readConfigurationFromFile() {
        try (XMLDecoder xmlDecoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("src/main/resources/config.xml")))) {
            LOG.debug("Configuration file is being read");
            return (Configuration) xmlDecoder.readObject();
        } catch (FileNotFoundException e) {
            LOG.error(e);
        }
        return null;
    }

    // Method to write configuration to XML file
    public static void writeConfigurationFile() {
        try (XMLEncoder xmlEncoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("src/main/resources/config.xml")))) {
            int[] integerArray = new int[]{1, 2, 3, 4, 5};
            Configuration configuration = new Configuration(10, 5, integerArray);
            xmlEncoder.writeObject(configuration);
            LOG.debug("Configuration file is written");
            xmlEncoder.flush();
        } catch (FileNotFoundException e) {
            LOG.error(e);
        }
    }
}
